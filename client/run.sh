#!/bin/bash

#RUN_AI="python python/game.py"

# gets the stored directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# check to see if no build parameter is passed
getopts "n" option
if [ $option != n ]; then
    echo Build Starting...
    cd $DIR/java
    mvn clean install -q # mute maven
    cd $DIR
fi

echo 'Make sure' `pwd` 'is the directory that run.sh is in.'

# for Java, uncomment this line:
RUN_AI="java -jar java/target/AWAP-0.0.1-SNAPSHOT.jar"

# for CPP, uncomment this line:
# RUN_AI="./cpp/game"

TEAM_ID="loop988"
FAST=0

trap "pkill -P $$" SIGTERM SIGKILL EXIT

while getopts "ft:" option;
do
    case $option in
        f) FAST=1 ;;
        t) TEAM_ID=$OPTARG ;;
    esac
done

python client.py "$RUN_AI" $TEAM_ID $FAST &

wait
