package awap;

/**
 * A class to help us rank moves
 *
 * Rankings are ints ranging from 0-100
 */
public class Rank implements Comparable<Rank> {
	private int rank;

	public Rank() {
		rank = 0;
	}

	public Rank(int rank) {
		// Bound the rank by 0 and 100
		this.rank = (int) Math.min(Math.max(0, rank), 100);
	}

	/**
	 * This constructor takes a value from 0-1
	 * @param val
	 */
	public Rank(double val) {
		rank = (int) Math.min(Math.max(0, val), 100);
	}

	public int getRank() {
		return rank;
	}

	public double getDoubleRank() {
		return (double) rank / 100.0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Rank)) return false;
		return (this.getRank() == ((Rank) o).getRank());
	}

	@Override
	public int compareTo(Rank o) {
		if(o == null)
			throw new NullPointerException("Can't compare a Rank to null");
		return this.getRank() - o.getRank();
	}
}
