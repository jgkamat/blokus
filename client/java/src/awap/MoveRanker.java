package awap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by jay on 10/25/14.
 */
public class MoveRanker {
	private ArrayList<Move> moves;
	private Random r = new Random();

	/**
	 * A constructor that takes in a ArrayList of moves
	 * @param in
	 */
	public MoveRanker(ArrayList<Move> in) {
		moves = in;
	}

	/**
	 * Will return a ArrayList with the same length as the moves filled with ranks!
	 * @return
	 */
	public ArrayList<Rank> rankAll() {
		if (moves == null)
			throw new NullPointerException();

		// An arraylist that will be returned with ranks
		ArrayList<Rank> ans = new ArrayList<Rank>(moves.size());
		//TODO Add code to rank all the moves
		
		for (int i = 0; i < moves.size(); i++) {
			Block tempBlock = Game.blocks.get(moves.get(i).getIndex());
			double diagLenTemp = tempBlock.getOffsets().get(0).distance(
					tempBlock.getOffsets().get(tempBlock.getOffsets().size()-1));
			double distanceToMid = 100;
			for(int j=0; j<4; j++) {
				Move tempMove = new Move(moves.get(i).getIndex(), j, moves.get(i).getX(), moves.get(i).getY());
				for(int k=0; k<tempBlock.getOffsets().size(); k++) {
					Point currentPoint = new Point(tempMove.getX()
							+ tempBlock.getOffsets().get(k).getX(),
							tempMove.getY()
							+ tempBlock.getOffsets().get(k).getY());
					Point centerPoint = new Point(0,0);
					switch(Game.number) {
					case 0:
						centerPoint = new Point(Game.state.getDimension(), Game.state.getDimension());
						break;
					case 1:
						centerPoint = new Point(0, Game.state.getDimension());
						break;
					case 2:
						centerPoint = new Point(0, 0);
						break;
					case 3:
						centerPoint = new Point(Game.state.getDimension(), 0);
						break;
					}
					if(currentPoint.distance(centerPoint) < distanceToMid) {
						distanceToMid = currentPoint.distance(centerPoint);
					}
				}
			}
			Rank currentRank = new Rank((diagLenTemp*10)-(distanceToMid));
			
			ans.add(currentRank);
		}

		return ans;
	}


	public Move getBestMove() {
		ArrayList<Rank> ranks = rankAll();

		int bestRank=0;
		ArrayList<Move> bestRanks =  new ArrayList<Move>();

		for(Rank loopy: ranks) {
			if(loopy.getRank() > bestRank)
				bestRank = loopy.getRank();
		}

		for(int i = 0; i < ranks.size(); i++) {
			Rank loopy = ranks.get(i);
			if(loopy.getRank() == bestRank) {
				bestRanks.add(moves.get(i));
			}
		}
		if(bestRanks.size() > 0) {
			return bestRanks.get(r.nextInt(bestRanks.size()));
		} else {
			return moves.get(r.nextInt(moves.size()));
		}
	}
}
